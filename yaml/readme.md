# `kubectl` Quick start

## Storage

This is where you store your dataset & code on the Nautilus cluster.

To view storage (PersistentVolumeClaims/PVC):
```
kubectl get pvc
```

To create storage:
```
kubectl create -f pvc.yaml
```
(Update `pvc.yaml` to modify name & size)

## Pods

Use pods to debug code before submitting a training job. Pods are killed automatically after 6 hours.

To view running pods:
```
kubectl get pod
```

To create pod:
```
kubectl create -f debug-pod.yaml
```

To connect to pod:
```
kubectl exec -it debug-pod -- bash
```

To copy data to pod:
```
kubectl cp /path/to/file my-pod:/path/to/file
```

To copy data from pod to local:
```
kubectl cp my-pod:my-file my-file
```

To delete a pod:
```
kubectl delete pods debug-pod
```

### `code-server`
A [`code-server`](https://github.com/cdr/code-server) instance should be running on the pod by default. To connect:
```
kubectl port-forward debug-pod 8080
```
then visit <localhost:8080> from your local browser.

## Jobs

To submit a job: Modify line 4/13 of template `train_template.yaml` to job name/command, then save as `job-cfg.yaml` and run
```
kubectl create -f job-cfg.yaml
```

*todo*: Python script for job submission
