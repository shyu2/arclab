FROM pytorch/pytorch
RUN apt-get update
RUN apt-get install -y libsm6 libxext6 libxrender-dev libgl1-mesa-glx vim git curl wget
ADD https://gitlab.com/shyu2/arclab/-/raw/main/environment.yml /tmp/environment.yml
RUN conda env update -f /tmp/environment.yml
RUN apt-get -y install libglib2.0-0
RUN pip install torch == 1.8.0+cu111 torchvision == 0.9.0+cu111
RUN pip install torchsummary segmentation_models_pytorch matplotlib opencv-python kornia packaging scikit-image
RUN conda init bash
RUN echo "source activate" >> ~/.bashrc
ENV PATH /opt/conda/bin:$PATH

